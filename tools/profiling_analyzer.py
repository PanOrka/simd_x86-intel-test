from pathlib import Path
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from typing import Dict
import itertools
import statistics


def color_cycle():
    for color in itertools.cycle("rbgk"):
        yield color


class ProfilePlot:
    def __init__(self, dataset: Dict[str, int], data_name: str) -> None:
        plt.suptitle(data_name)
        plt.ylabel("TSC cycles")
        plt.xlabel("Test")
        plt.subplots_adjust(
            left=0.045,
            bottom=0.045,
            right=0.98,
            top=0.924,
            wspace=0.2,
            hspace=0.2,
        )
        self._color_generator = color_cycle()

        self._set_dataset(dataset)

    def _set_dataset(self, dataset: Dict[str, int]) -> None:
        sorted_items = sorted(list(dataset.items()), key=lambda x: int(x[0].split("_")[0]))
        sorted_list_of_tests = sorted(list(set(["_".join(item[0].split("_")[1:]) for item in sorted_items])))
        offset = len(sorted_list_of_tests)

        idx_start = 0
        end_lst = []
        while idx_start < len(sorted_items):
            end_lst += sorted(sorted_items[idx_start:idx_start + offset], key=lambda x: x[0])
            idx_start += offset

        categories_num = int(len(end_lst) / offset)
        x_ticks = [x * offset for x in range(categories_num)]
        x_labels = [0, 1, 10, 100, 500, 1000, 2500, 5000, 10000]
        plt.xticks(x_ticks, x_labels)

        mean_vec = []
        k_vec = []
        ctr = 0
        for k, v in end_lst:
            color = next(self._color_generator)
            plt.plot([k] * len(v), v, ".", color=color)

            mean = statistics.mean(v)
            if ctr < offset:
                plt.plot(k, mean, "_", color=color, mew=2, ms=30, label=sorted_list_of_tests[ctr])
                ctr += 1
            else:
                plt.plot(k, mean, "_", color=color, mew=2, ms=30)

            mean_vec.append(mean)
            k_vec.append(k)
        plt.plot(k_vec, mean_vec, "-", color="k")

        plt.legend(bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left",
                   borderaxespad=0, ncol=3)

    def show(self) -> None:
        """
        After call to that function, the plot created
        through this class is discarded. So that can
        be used only once.
        """
        plt.show()
        plt.clf()


class ProfilingAnalyzer:
    def __init__(self) -> None:
        self._data = dict()
        self._data_name = None

    def _add_data(self, f_path: Path) -> None:
        algo_name = f_path.name

        with f_path.open("r") as data_file:
            while True:
                try:
                    time = int(data_file.readline().replace("TSC:", "").rstrip())
                except ValueError:
                    break

                if algo_name not in self._data:
                    self._data[algo_name] = []
                self._data[algo_name].append(time)

    def _show_plot(self) -> None:
        plt = ProfilePlot(self._data, self._data_name)
        plt.show()

    def start_analyze(self) -> None:
        src = Path(input("Input path to directory of data sets: "))
        if not src.is_dir():
            print("Provided path is not a directory!")
            return

        for f_path in src.iterdir():
            self._add_data(f_path)

        self._data_name = src.name
        self._show_plot()


if __name__ == "__main__":
    while (True):
        ProfilingAnalyzer().start_analyze()
