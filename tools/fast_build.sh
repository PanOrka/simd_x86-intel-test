#! /bin/bash
set -Eeuo pipefail

GIT_ROOT_DIR="$(git rev-parse --show-toplevel)"
BUILD_DIR="${GIT_ROOT_DIR}""/build"
FAST_BUILD_DIR="${GIT_ROOT_DIR}""/fast_build"
ITER_SETTING_FILE="${GIT_ROOT_DIR}""/src/common/setting.hpp"

rm -rf "${FAST_BUILD_DIR}"
mkdir -p "${FAST_BUILD_DIR}"

copy_installed() {
    local SUFFIX="${1}"

    declare -a EXEC_NAMES_ARRAY=( $(ls "${SX86IT_INSTALL_DIR}") )
    for value in "${EXEC_NAMES_ARRAY[@]}"; do
        cp "${SX86IT_INSTALL_DIR}""/${value}" "${FAST_BUILD_DIR}""/${value}_${SUFFIX}"
    done
}

single_build() {
    local OPTIONS="${1}"
    local SUFFIX="${2}"

    rm -rf "${BUILD_DIR}"
    mkdir -p "${BUILD_DIR}" && cd "${BUILD_DIR}"
    cmake .. "${OPTIONS}"
    make install-all -j$(nproc)

    copy_installed "${SUFFIX}"
}

declare -a ITER_NUM_ARRAY=(0 1 10 100 500 1000 2500 5000 10000)
for iter in "${ITER_NUM_ARRAY[@]}"; do
    echo "Compiling iter ${iter}"
    sed -i "s/iter_amount = .*;/iter_amount = ${iter};/" "${ITER_SETTING_FILE}"
    single_build "-DCOMPILE-CLANG-13.0.0=ON" "clang_""${iter}" &> /dev/null
    single_build "-DCOMPILE-GCC-11.2.0=ON" "gcc_""${iter}" &> /dev/null
done
echo "Done. Executables are in directory: ${FAST_BUILD_DIR}"
