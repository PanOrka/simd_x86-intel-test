#! /bin/bash
set -Eeuo pipefail

if [ $# -ne 1 ] ; then
    echo "Please provide root path for SDK-sx86it directory."
    exit 1
fi

ROOT_DIR="$(cd "${1}" && pwd)"

SELF_DIR="$(dirname "$(readlink -f "${0}")")"
GIT_ROOT_DIR="$(git rev-parse --show-toplevel)"
DEPLOY_DIR="${GIT_ROOT_DIR}""/deploy"
PREPARE_FUNC="${DEPLOY_DIR}""/prepare_functions.sh"
DOCKER_REGISTRY="${DEPLOY_DIR}""/DOCKER_REGISTRY"

SDK_DIR="${ROOT_DIR}""/SDK-sx86it"
echo "SDK directory is: ${SDK_DIR}"
mkdir -p "${SDK_DIR}"

COMPILERS_DIR="${SDK_DIR}""/compilers-sx86it"
echo "Compilers directory is: ${COMPILERS_DIR}"
mkdir -p "${COMPILERS_DIR}"

SYSROOT_DIR="${SDK_DIR}""/sysroot"
echo "Sysroot directory is: ${SYSROOT_DIR}"
mkdir -p "${SYSROOT_DIR}"

INSTALL_DIR="${SDK_DIR}""/bin"
echo "Install directory is: ${INSTALL_DIR}"
mkdir -p "${INSTALL_DIR}"

download_package() {
    local URL="${1}"

    if wget -q --spider "${URL}"; then
        wget --progress=dot -e dotbytes=1M --directory-prefix="${COMPILERS_DIR}" "${URL}"
        echo "${COMPILERS_DIR}""/$(basename ${URL})"
    else
        echo "${URL} is unavailable!"
        exit 1
    fi
}

unpack_package() {
    local ARCHIVE_LOC="${1}"
    local ROOT_DIRNAME="${2}"
    local ROOT_DIR="${COMPILERS_DIR}""/${ROOT_DIRNAME}"

    rm -rf "${ROOT_DIR}"
    mkdir -p "${ROOT_DIR}"
    tar -xf "${ARCHIVE_LOC}" -C "${ROOT_DIR}"

    echo "${ROOT_DIR}""/$(ls "${ROOT_DIR}")"
}

get_clang() {
    local CLANG_13_URL="https://github.com/llvm/llvm-project/releases/download/llvmorg-13.0.0/clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-20.04.tar.xz"
    local CLANG_13_ARCHIVE="$(download_package "${CLANG_13_URL}")"
    local CLANG_13_ROOT_DIRNAME="clang13.0.0"

    echo "unpacking ${CLANG_13_ROOT_DIRNAME}..."
    CLANG_13_DIR="$(unpack_package "${CLANG_13_ARCHIVE}" "${CLANG_13_ROOT_DIRNAME}")"
    echo "${CLANG_13_ROOT_DIRNAME} location is: ${CLANG_13_DIR}"

    rm -rf "${CLANG_13_ARCHIVE}"
}

get_clang

get_gcc() {
    local GCC_11_URL="https://gitlab.com/PanOrka/sx86it-docker-registry/-/raw/master/gcc-11.2.0-x86_64-linux-gnu-ubuntu-20.04.tar.xz"
    local GCC_11_ARCHIVE="$(download_package "${GCC_11_URL}")"
    local GCC_11_ROOT_DIRNAME="gcc11.2.0"

    echo "unpacking ${GCC_11_ROOT_DIRNAME}..."
    GCC_11_DIR="$(unpack_package "${GCC_11_ARCHIVE}" "${GCC_11_ROOT_DIRNAME}")"
    echo "${GCC_11_ROOT_DIRNAME} location is: ${GCC_11_DIR}"

    rm -rf "${GCC_11_ARCHIVE}"
}

get_gcc

get_sysroot() {
    source "${PREPARE_FUNC}"

    echo "Preparing image..."
    IMAGE="$(prepare_image)"
    echo "Image is: ${IMAGE}"
    local CONTAINER="$(docker create "${IMAGE}")"
    local IMAGE_TAR="${SYSROOT_DIR}""/image.tar"
    docker export "${CONTAINER}" > "${IMAGE_TAR}"
    tar -xf "${IMAGE_TAR}" -C "${SYSROOT_DIR}"

    rm -rf "${IMAGE_TAR}"
}

get_sysroot

cat << EOF > "${SDK_DIR}""/env_setup"
export NONSDK_PS1="\${PS1}"
export PS1="(sdk:sx86it) \${NONSDK_PS1}"
export SX86IT_SYSROOT="${SYSROOT_DIR}"
export SX86IT_IMAGE="${IMAGE}"
export SX86IT_CLANG_XX_13="${CLANG_13_DIR}/bin/clang++"
export SX86IT_GCC_XX_11="${GCC_11_DIR}/bin/g++"
export SX86IT_INSTALL_DIR="${INSTALL_DIR}"
llvm-mca-profile() {
    "${CLANG_13_DIR}"/bin/llvm-mca --march=x86-64 --mcpu=skylake -timeline -bottleneck-analysis "\${1}"
}
EOF
