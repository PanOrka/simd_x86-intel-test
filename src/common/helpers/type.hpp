#ifndef HELPERS_TYPE_HPP
#define HELPERS_TYPE_HPP

#include <type_traits>

namespace sx86it {

/**
 * Helpful when we need to ensure that an address of first member
 * of a struct is the same as an address of the struct.
 *
 * The wrapper is needed because C++ doesn't allow
 * to do that static_assert inside of the struct
 * because it is an incomplete type there.
*/
template <typename T>
struct standard_layout_type_wrapper {
    using type = T;
    static_assert(std::is_standard_layout_v<type>);
};

}

#endif
