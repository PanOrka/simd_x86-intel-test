#ifndef HELPERS_TIME_PROFILING
#define HELPERS_TIME_PROFILING

#include <x86intrin.h>
#include <iostream>

namespace sx86it {

[[gnu::always_inline]] auto read_tsc() noexcept {
    _mm_mfence();
    _mm_lfence();
    auto tsc = __rdtsc();
    _mm_lfence();

    return tsc;
}

template <int WarmUpIterations, typename Func, typename ...Args>
[[gnu::noinline]] void time_profile(volatile Func f, Args&& ...args) noexcept( noexcept(f(std::forward<Args>(args)...)) ) {
    for (decltype(WarmUpIterations) i{}; i < WarmUpIterations; ++i) {
        f(std::forward<Args>(args)...);
    }

    auto start = read_tsc();
    f(std::forward<Args>(args)...);
    auto stop = read_tsc();
    auto diff = stop - start;

    std::cout << "TSC:" << diff << std::endl;
}

template <int WarmUpIterations, auto Func, typename ...Args>
[[gnu::noinline]] void time_profile_inline(Args&& ...args) noexcept( noexcept(Func(std::forward<Args>(args)...)) ) {
    for (decltype(WarmUpIterations) i{}; i < WarmUpIterations; ++i) {
        [[maybe_unused]] volatile auto ret_val = Func(std::forward<Args>(args)...);
    }

    auto start = read_tsc();
    [[maybe_unused]] volatile auto ret_val = Func(std::forward<Args>(args)...);
    auto stop = read_tsc();
    auto diff = stop - start;

    std::cout << "TSC:" << diff << std::endl;
}

}

#endif
