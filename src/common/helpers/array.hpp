#ifndef HELPERS_ARRAY_HPP
#define HELPERS_ARRAY_HPP

#include <cstdint>

namespace sx86it {

template <typename T, std::size_t N>
constexpr auto array_size(const T(&)[N]) noexcept {
    return N;
}

}

#endif
