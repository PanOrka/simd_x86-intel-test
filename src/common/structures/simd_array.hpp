#ifndef STRUCTURES_SIMD_ARRAY_HPP
#define STRUCTURES_SIMD_ARRAY_HPP

#include "common/helpers/type.hpp"

#include <cstdint>
#include <cstddef>

namespace sx86it {

/**
 * WARNING: Use that type only through simd_array typedef!
*/
template <typename T, std::size_t N>
struct alignas(64) aligned_array {
public:
    using size_type = std::size_t;
    using value_type = T;

    static constexpr size_type size{N};

private:
    std::byte data[sizeof(value_type) * size]{};

public:
    constexpr value_type& operator[](size_type idx) & noexcept {
        return const_cast<value_type&>(const_cast<const aligned_array&>(*this)[idx]);
    }

    constexpr const value_type& operator[](size_type idx) const& noexcept {
        return reinterpret_cast<const value_type*>(&this->data[0])[idx];
    }

};

template <typename T, std::size_t N>
using simd_array = typename standard_layout_type_wrapper<aligned_array<T, N>>::type;

using simd_array_ui32 = simd_array<uint32_t, 64>;
using simd_array_i32 = simd_array<int32_t, 64>;

using simd_array_ui64 = simd_array<uint64_t, 32>;
using simd_array_i64 = simd_array<int64_t, 32>;

using simd_array_f32 = simd_array<float, 64>;
using simd_array_f64 = simd_array<double, 32>;

}

#endif
