#include "common/structures/simd_array.hpp"
#include "common/setting.hpp"
#include "common/helpers/time_profiling.hpp"

#include <immintrin.h>
#include <type_traits>

namespace sx86it {

// we assume that mul and add will be okay to fit int32_t in c
[[gnu::always_inline]] int add_mul(simd_array_i32& __restrict__ a, simd_array_i32& __restrict__ b, simd_array_i32& __restrict__ c) noexcept {
    asm volatile("# LLVM-MCA-BEGIN add_mul");
    static constexpr auto array_size = simd_array_i32::size;

    for (int idx{}; idx < array_size; ++idx) {
        c[idx] = a[idx] * b[idx];
        c[idx] = c[idx] + b[idx];
    }
    asm volatile("# LLVM-MCA-END");

    return 0;
}

}

int main() {
    sx86it::simd_array_i32 a{}, b{}, c{};

    for (int i{}; i < decltype(a)::size; ++i) {
        a[i] = b[i] = i;
    }

    sx86it::time_profile_inline<sx86it::iter_amount, sx86it::add_mul>(a, b, c);
}
