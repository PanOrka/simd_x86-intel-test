#include "common/structures/simd_array.hpp"
#include "common/setting.hpp"
#include "common/helpers/time_profiling.hpp"

#include <immintrin.h>
#include <type_traits>

namespace sx86it {

// we assume that mul and add will be okay to fit int32_t in c
[[gnu::always_inline]] int add_mul_intrin(simd_array_i32& __restrict__ a, simd_array_i32& __restrict__ b, simd_array_i32& __restrict__ c) noexcept {
    asm volatile("# LLVM-MCA-BEGIN add_mul_intrin");
    static constexpr auto offset = 8;
    static constexpr auto array_size = simd_array_i32::size;

    static_assert(array_size % offset == 0);
    static constexpr int N{array_size / offset};

    for (int step{}; step < N; ++step) {
        const auto idx = step * offset;

        auto& __restrict__ ai = reinterpret_cast<__m256i&>(a[idx]);
        auto& __restrict__ bi = reinterpret_cast<__m256i&>(b[idx]);
        auto& __restrict__ ci = reinterpret_cast<__m256i&>(c[idx]);

        auto mul_prod = _mm256_mullo_epi32(ai, bi);
        ci = _mm256_add_epi32(mul_prod, bi);
    }
    asm volatile("# LLVM-MCA-END");

    return 0;
}

}

int main() {
    sx86it::simd_array_i32 a{}, b{}, c{};

    for (int i{}; i < decltype(a)::size; ++i) {
        a[i] = b[i] = i;
    }

    sx86it::time_profile_inline<sx86it::iter_amount, sx86it::add_mul_intrin>(a, b, c);
}
