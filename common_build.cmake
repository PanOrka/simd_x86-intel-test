function (build_target_arch_avx512 target)
    target_compile_options(${target} PRIVATE
        --sysroot=${SYSROOT_DIR}
        -march=${TARGET_ARCH_AVX512}
        -O3
        -fpie
    )

    if(SAVE-TEMPS)
        target_compile_options(${target} PRIVATE
            -save-temps
        )
    endif()

    target_link_options(${target} PRIVATE
        "-Wl,-dynamic-linker,/usr/lib64/ld-linux-x86-64.so.2"
        "-pie"
    )

    install(TARGETS ${target} DESTINATION ${INSTALL_DESTINATION})
    add_dependencies(install-all ${target})

endfunction()
