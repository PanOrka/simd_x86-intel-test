#! /bin/bash
set -Eeuo pipefail

SELF_DIR="$(dirname "$(readlink -f "${0}")")"

COREMASK_PATH="${SELF_DIR}""/COREMASK"
source "${COREMASK_PATH}"

PREPARE_FUNC="${SELF_DIR}""/prepare_functions.sh"
source "${PREPARE_FUNC}"

disable_IRQs
set_cpufreq_governor
