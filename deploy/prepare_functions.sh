prepare_image() {
    local IMAGE_TMP_DIR="$(mktemp -d)"
    local IMAGE_URL="$(cat "${DOCKER_REGISTRY}")"

    if wget -q --spider "${IMAGE_URL}"; then
        wget --directory-prefix="${IMAGE_TMP_DIR}" "${IMAGE_URL}"
        local IMAGE_PATH="${IMAGE_TMP_DIR}""/$(basename ${IMAGE_URL})"
    else
        echo "${IMAGE_URL} is unavailable!"
        exit 1
    fi

    local PREFIX="Loaded image: "
    local LOAD_RET="$(docker load -i "${IMAGE_PATH}")"

    echo "${LOAD_RET#"${PREFIX}"}"
}

convert_64_coremask_to_hex_str() {
    local COREMASK_STR="${1}"
    declare -a COREMASK_ARRAY=( $(echo "${COREMASK_STR}" | sed "s/./& /g") )

    declare -a HEX_COREMASK_ARRAY=( $(for i in {0..16}; do echo -n "0 "; done;) )
    HEX_COREMASK_ARRAY[8]=","

    for idx_hex in {0..7}; do
        local IDX_LOW=$((idx_hex*4))
        local IDX_HIGH=$((33 + idx_hex*4))
        local BIT_4_LOW=${COREMASK_ARRAY[@]:${IDX_LOW}:4}
        local BIT_4_HIGH=${COREMASK_ARRAY[@]:${IDX_HIGH}:4}

        local BIT_4_LOW_STRING="$(for val in ${BIT_4_LOW[@]}; do echo -n "${val}"; done;)"
        local BIT_4_HIGH_STRING="$(for val in ${BIT_4_HIGH[@]}; do echo -n "${val}"; done;)"

        local HEX_LOW_DEST=${idx_hex}
        local HEX_HIGH_DEST=$((9 + ${idx_hex}))

        HEX_COREMASK_ARRAY[${HEX_LOW_DEST}]="$(printf "%x" "$((2#"${BIT_4_LOW_STRING}"))")"
        HEX_COREMASK_ARRAY[${HEX_HIGH_DEST}]="$(printf "%x" "$((2#"${BIT_4_HIGH_STRING}"))")"
    done

    echo "$(for val in ${HEX_COREMASK_ARRAY[@]}; do echo -n "${val}"; done;)"
}

calculate_invert_64_coremask_str() {
    local COREMASK_STR="${1}"
    declare -a COREMASK_ARRAY=( $(echo "${COREMASK_STR}" | sed "s/./& /g") )

    for indice in ${!COREMASK_ARRAY[@]}; do
        if (( ${indice} == 32 )); then
            continue
        elif (( ${COREMASK_ARRAY[${indice}]} == 1 )); then
            COREMASK_ARRAY[${indice}]=0
        elif (( ${COREMASK_ARRAY[${indice}]} == 0 )); then
            COREMASK_ARRAY[${indice}]=1
        fi
    done

    echo "$(for val in ${COREMASK_ARRAY[@]}; do echo -n "${val}"; done;)"
}

convert_64_coremask_str_to_cpu_list() {
    local COREMASK_STR="${1}"
    local COREMASK_STR_REV="$(echo "${COREMASK_STR}" | rev)"

    declare -a COREMASK_REV_ARRAY=( $(echo "${COREMASK_STR_REV}" | sed "s/./& /g") )

    declare -a CORELIST=( )

    for (( idx=0;idx<64;idx++ )); do
        local indice=0
        if [ ${idx} -ge 32 ]; then
            indice=$(( ${idx} + 1 ))
        else
            indice=${idx}
        fi

        if (( ${COREMASK_REV_ARRAY[${indice}]} == 1 )); then
            if (( ${#CORELIST[@]} >= 1 )); then
                CORELIST+=( ",""${idx}" )
            else
                CORELIST+=( "${idx}" )
            fi
        fi
    done

    echo "$(for val in ${CORELIST[@]}; do echo -n "${val}"; done;)"
}

calculate_64_coremask_str() {
    declare -a COREMASK_ARRAY=( $(for i in {0..64}; do echo -n "0 "; done;) )
    COREMASK_ARRAY[32]=","

    local CORE_LIST="${1}"
    declare -a CORE_LIST_SPLIT=( $(echo "${CORE_LIST}" | sed "s/,/ /g") )

    for cores in ${CORE_LIST_SPLIT[@]}; do
        if echo "${cores}" | grep -q "-"; then
            declare -a RANGE=( $(echo "${cores}" | sed "s/-/ /") )

            for ((idx=${RANGE[0]};idx<=${RANGE[1]};idx++)); do
                if [ ${idx} -ge 32 ]; then
                    COREMASK_ARRAY[$(( ${idx} + 1 ))]="1"
                else
                    COREMASK_ARRAY[${idx}]="1"
                fi
            done
        else
            if [ ${cores} -ge 32 ]; then
                COREMASK_ARRAY[$(( ${cores} + 1 ))]="1"
            else
                COREMASK_ARRAY[${cores}]="1"
            fi
        fi
    done

    echo "$(for val in ${COREMASK_ARRAY[@]}; do echo -n "${val}"; done;)" | rev
}

set_kernel_default_arg() {
    local GRUB_LOCATION="/etc/default/grub"
    local ARG="${1}"
    local VALUE="${2}"

    if grep -q "GRUB_CMDLINE_LINUX_DEFAULT=.*${ARG}=" "${GRUB_LOCATION}"; then
        sudo sed -i "s/${ARG}=[^[:blank:]]* /${ARG}=${VALUE} /" "${GRUB_LOCATION}"
        sudo sed -i "s/${ARG}=[^[:blank:]]*\"/${ARG}=${VALUE}\"/" "${GRUB_LOCATION}"
    elif grep -q "GRUB_CMDLINE_LINUX_DEFAULT=" "${GRUB_LOCATION}"; then
        sudo sed -i "s/GRUB_CMDLINE_LINUX_DEFAULT=\"/&${ARG}=${VALUE} /" "${GRUB_LOCATION}"
    else
        sudo sed -i "$ a GRUB_CMDLINE_LINUX_DEFAULT=\"${ARG}=${VALUE}\""
    fi
}

isolate_cpus() {
    local CPUS="${1}"

    local GRUB_LOCATION="/etc/default/grub"
    if [ ! -f "${GRUB_LOCATION}" ]; then
        echo "Change script to use correct grub file location, current is: ${GRUB_LOCATION}"
        exit 1
    fi

    echo "Backup of grub file to stdout:"
    echo "======================================================="
    cat "${GRUB_LOCATION}"
    echo "======================================================="

    echo "Calculating coremask..."
    local BINARY_COREMASK_STR="$(calculate_64_coremask_str "${CPUS}")"
    local BINARY_INVERTED_COREMASK_STR="$(calculate_invert_64_coremask_str "${BINARY_COREMASK_STR}")"
    local HEX_COREMASK_STR="$(convert_64_coremask_to_hex_str "${BINARY_COREMASK_STR}")"
    local HEX_INVERTED_COREMASK_STR="$(convert_64_coremask_to_hex_str "${BINARY_INVERTED_COREMASK_STR}")"
    echo "Used CPU masks: {"
    echo "    BINARY_COREMASK=          ${BINARY_COREMASK_STR}"
    echo "    BINARY_INVERTED_COREMASK= ${BINARY_INVERTED_COREMASK_STR}"
    echo "    HEX_COREMASK=             ${HEX_COREMASK_STR}"
    echo "    HEX_INVERTED_COREMASK=    ${HEX_INVERTED_COREMASK_STR}"
    echo "}"

    echo "Changing kernel parameters..."
    local ISOLCPU_ARG="isolcpus"
    local NOHZ_FULL="nohz_full"
    local NOHZ="nohz"
    local IRQ_DEFAULT_AFFINITY="irqaffinity"

    set_kernel_default_arg "${ISOLCPU_ARG}" "${CPUS}"
    set_kernel_default_arg "${NOHZ_FULL}" "${CPUS}"
    set_kernel_default_arg "${NOHZ}" "on"
    set_kernel_default_arg "${IRQ_DEFAULT_AFFINITY}" "$(convert_64_coremask_str_to_cpu_list "${BINARY_INVERTED_COREMASK_STR}")"
    sudo update-grub

    local COREMASK_PATH="${SELF_DIR}""/COREMASK"
cat << EOF > "${COREMASK_PATH}"
export CPULIST="${CPUS}"
export BINARY_COREMASK="${BINARY_COREMASK_STR}"
export BINARY_INVERTED_COREMASK="${BINARY_INVERTED_COREMASK_STR}"
export HEX_COREMASK="${HEX_COREMASK_STR}"
export HEX_INVERTED_COREMASK="${HEX_INVERTED_COREMASK_STR}"
EOF

}

disable_IRQs() {
    echo "Disabling IRQ balancer..."
    systemctl stop irqbalance &> /dev/null || true

    echo "Changing IRQ coremasks..."
    echo "${HEX_INVERTED_COREMASK}" | sudo tee /proc/irq/*/smp_affinity &> /dev/null || true
    echo "${HEX_INVERTED_COREMASK}" | sudo tee /proc/irq/default_smp_affinity &> /dev/null || true
}

set_cpufreq_governor() {
    echo "Enabling performance cpu governor scaling mode..."
    declare -a COREMASK_REV_ARRAY=( $(echo "${BINARY_COREMASK}" | sed "s/./& /g" | rev) )
    declare -a CORELIST=()

    for (( idx=0;idx<64;idx++ )); do
        local indice=0
        if [ ${idx} -ge 32 ]; then
            indice=$(( ${idx} + 1 ))
        else
            indice=${idx}
        fi

        if (( ${COREMASK_REV_ARRAY[${indice}]} == 1 )); then
            CORELIST+=( "${idx}" )
        fi
    done

    for cpu in "${CORELIST[@]}"; do
        echo "performance" | sudo tee /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_governor &> /dev/null || true
    done
}
