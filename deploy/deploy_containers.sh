#! /bin/bash
set -Eeuo pipefail

SELF_DIR="$(dirname "$(readlink -f "${0}")")"

docker rm --force sx86it || true
docker create --privileged --volume "${SELF_DIR}""/bin":/opt/sx86it/bin --tty --name sx86it "$(cat "${SELF_DIR}""/DOCKER_IMAGE")"

docker cp "${SELF_DIR}""/start_process.sh" sx86it:/opt/sx86it/

docker start sx86it
