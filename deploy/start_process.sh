#! /bin/bash

# this runs specified program in ${2} on cpu-list ${1}
# format of 1 should be same as isolcpus kernel parameter
taskset --cpu-list -p "${1}" "${$}" &> /dev/null
exec "${2}"
