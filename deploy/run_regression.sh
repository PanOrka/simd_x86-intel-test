#! /bin/bash
set -Eeuo pipefail

if [ $# -ne 1 ] ; then
    echo "Please provide amount of tests."
    exit 1
fi

TEST_AMOUNT="${1}"

SELF_DIR="$(dirname "$(readlink -f "${0}")")"
OUT_DIR="${SELF_DIR}""/OUT"
COREMASK_PATH="${SELF_DIR}""/COREMASK"
source "${COREMASK_PATH}"

RUN_SCRIPT="/opt/sx86it/start_process.sh"
EXEC_DIR="/opt/sx86it/bin"

rm -rf "${OUT_DIR}"
mkdir -p "${OUT_DIR}"

exec_cmd() {
    local CMD="${1}"

    docker exec sx86it bash -c "${CMD}"
}

declare -a EXEC_NAMES_ARRAY=( $(exec_cmd "ls "${EXEC_DIR}"") )
for value in "${EXEC_NAMES_ARRAY[@]}"; do
    echo "Executing ${value}..."
    for (( idx=0;idx<${TEST_AMOUNT};idx++ )); do
        RET="$(exec_cmd ""${RUN_SCRIPT}" "${CPULIST}" "${EXEC_DIR}""/${value}"")"
        echo "${RET}" >> "${OUT_DIR}""/${value}"
    done
done
