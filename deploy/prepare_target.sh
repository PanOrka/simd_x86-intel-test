#! /bin/bash
set -Eeuo pipefail

SELF_DIR="$(dirname "$(readlink -f "${0}")")"
DOCKER_REGISTRY="${SELF_DIR}""/DOCKER_REGISTRY"
PREPARE_FUNC="${SELF_DIR}""/prepare_functions.sh"
source "${PREPARE_FUNC}"

echo "Preparing image..."
IMAGE="$(prepare_image)"
echo "Image is: ${IMAGE}"
echo "${IMAGE}" > "${SELF_DIR}""/DOCKER_IMAGE"

prepare_target() {
    local ISOLATED_CPU_LIST="1" # format as in isolcpus kernel
                                # parameter should be used,
                                # will not be validated
                                # so use correct list or UB
                                # 1-2,4,6-7 means take 1,2,4,6,7

    isolate_cpus "${ISOLATED_CPU_LIST}"
}

echo "Preparing target machine..."
prepare_target

echo "Please reboot the machine for changes to take effect."
