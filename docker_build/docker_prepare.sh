#! /bin/bash
set -Eeuo pipefail

SELF_DIR="$(dirname "$(readlink -f "${0}")")"
(
    cd "${SELF_DIR}"
    BASE_IMAGE_VERSION="./BASE_IMAGE_VERSION"
    SYSROOT_IMAGE_VERSION="./SYSROOT_IMAGE_VERSION"

    IMAGE="$(cat "${BASE_IMAGE_VERSION}")"
    NEW_TAG="$(cat "${SYSROOT_IMAGE_VERSION}")"

    docker pull "${IMAGE}"
    docker tag "${IMAGE}" ubuntu-focal-sx86it-base
    docker build ./ --force-rm=true --tag="${NEW_TAG}"

    echo "${NEW_TAG} container is prepared!"
)
