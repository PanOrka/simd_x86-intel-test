# SIMD_x86-intel-test

x86 Intel SIMD compiler test + intrinsics

# Prerequisites

* **Python 3.8 and matplotlib 3.4.3**
* **GNU bash**
* **CMake 3.10+**
* **Docker**

**Host:** x86-64_linux-gnu<br>
**Target:** skylake-avx512 powered cpu x86-64_linux-gnu<br>

**Tested only on Ubuntu OS:**<br>
**Host:** Ubuntu 20.04, 5.4.0-89-generic, generic-cpu<br>
**Target:** Ubuntu 18.04, 4.19.0-6-amd64, SMP Debian 4.19.67-2, Intel(R) Xeon(R) Gold 6338N CPU @ 2.20GHz<br>

## Warnings

**I provide no warranty for the software on this repository nor take responsibility for any harm that this software can cause.**

**Read the scripts and think whether it can cause any problems with your setup...**

**It's best to test it on VM isolated environment, like I do and It works for me.**

## How to use

We provide SDK with compilers and container's sysroots, compilers are native x86-64_linux-gnu for both host and target machines.

Download SDK:
```
<sx86it-repo-root>/tools/makesdk.sh <existing-directory-root-for-SDK>
```

SDK consists of:
* clang13.0.0 prebuilt binaries,
* gcc11.2.0 prebuilt binaries,
* execution environment sysroots of used docker container.

Source environment setup:
```
source <SDK-root>/SDK-sx86it/env_setup
```

Build application:
```
cd <sx86it-repo-root>
mkdir build && cd build
cmake .. -D<compiler-option>=ON && make install-all
```

Where `<compiler-option>` is one of:
* `COMPILE-GCC-11.2.0`
* `COMPILE-CLANG-13.0.0`

All targets will be built with specified compiler and installed in SDK directory:
```
<SDK-root>/SDK-sx86it/bin
```

That directory can be also accessed with environmental variable `SX86IT_INSTALL_DIR`.

After successful build we need to prepare target. Copy deploy folder with needed scripts to target:
```
scp -rF <ssh-config> <sx86it-repo-root>/deploy <target>:<deploy-root>
```

And prepare target with `prepare_target.sh` script.
```
<deploy-root>/deploy/prepare_target.sh
```

`prepare_target.sh` does several things to your machine:
* downloads docker image from url specified in `DOCKER_REGISTRY`,
* isolates cpu by kernel cmdline arguments: `isolcpus` (userspace threads), `nohz`, `nohz_full` (LOC interrupts), `irqaffinity` with corelist specified in `prepare_target.sh` (default is `core=0`). The script assumes that we can change that through `/etc/default/grub` file, you need to check manually whether some of the options specified them are contradictory, the script only performs regex swap or insert of specified arguments above. Additionally it will print backup of `/etc/default/grub` to stdout.

**NOTE:** `prepare_target.sh` needs write permissions in `<deploy-root>/deploy` directory, because it saves information about isolated cores and docker image for the rest of scripts. Additionally it may need root permissions for editing some of the files.

After successful execution of `prepare_target.sh` reboot your system:
```
sudo reboot
```

Changes made to the system by `prepare_target.sh` are persistent between boots, but we need to do some additional things that should be repetead after every boot. The things can be done with `after_boot.sh` script:
```
<deploy-root>/deploy/after_boot.sh
```

`after_boot.sh` does several things to your machine:
* disables `irqbalance` sysctl service,
* disables IRQ allocation for isolated core through `/proc/irq/*/smp_affinity` and `/proc/irq/default_smp_affinity`,
* sets up `scaling_governor` to `performance` mode for isolated cores, It doesn't set cpu frequency to constant value, but it is more than enough on x86-64 systems, It will let 100% cpu load without caring about power-savings.

After that the target machine is prepared for our application. From now on we can deploy docker containers and run our regression for all compiled binaries.

Firstly we need to upload our binaries to the target:
```
scp -rF <ssh-config> "${SX86IT_INSTALL_DIR}" <target>:<deploy-root>/deploy
```

After that we can deploy container:
```
<deploy-root>/deploy/deploy_containers.sh
```

**NOTE:** `deploy_containers.sh` maps `<deploy-root>/deploy/bin` to `/opt/sx86it/bin` and copies `start_process.sh` to specific place.

And finally we can run our regression:
```
<deploy-root>/deploy/run_regression.sh
```

The script will run our programs **only on isolated core-list (It's best to use only single core in isolation)** and will create `OUT` file in `<deploy-root>/deploy` directory. The file contains our output of benchmark for each application in TSC as time units for generic and intrinsic implementation in specified order. The files can be compared between each other with `<sx86it-repo-root>/tools/profiling_analyzer.py` script.

## How to use `profiling_analyzer.py`

**Prerequisites:**
* python3,
* matplotlib.

`profiling_analyzer.py` is a CLI application:
```
python3 <sx86it-repo-root>/tools/profiling_analyzer.py
```

Insert desired option and proceed according to outputted information.

## Worth to know/do

### IRQs

`after_boot.sh` can have problems with isolating some of IRQs - just accept it... Ubuntu-Linux isn't RTOS and never will be.

It is possible to watch interrupts in real-time:
```
watch -n0.1 "cat /proc/interrupts | cut -c -$COLUMNS"
```

### How to check isolation

1. Check output of `taskset -cp 1` - that will retrieve the affinity of initial process and that tells us which cores **are not isolated**.
2. Check `cat /sys/devices/system/cpu/isolated` if list is the same as in `prepare_target.sh`.
3. Check if your kernel is tickless on running cpus:
    * `grep NO_HZ /boot/config-$(uname -r)` - we want `CONFIG_NO_HZ_FULL=y` otherwise LOC (local timer interrupt) will be executed every now and then, which can decrease overall performance of task running on isolated core and `nohz_full` kernel parameter will have no effect.

### Frequency

The scripts only enable `performance` mode through governor, but It's best to fix your cpu frequency to some value. It is not performed automatically, because Ubuntu doesn't provide native method to do so and It can be pretty dangerous especially in multi-core systems, where we have some power-consumption constraints basing on others CPU usage - that's why more advanced power management is left to the user.

It is possible to watch cpu frequencies in real-time:
```
watch -n.1 "grep \"^[c]pu MHz\" /proc/cpuinfo"
```

### TSC

It's goot to check your TSC frequency:
```
sudo dmesg | grep tsc
```

TSC frequency (closer to)/(higher than) our isolated CPU frequency means better - benchmark uses TSC to define "performance" of the application, so we want high-resolution timer that can count everything precisely. Normally TSC will have nominal cpu frequency especially when `constant_tsc` cpu flag is available.

`constant_tsc` flag isn't that important here, because all benchmarks should be executed on single core **always**, no rescheduling involved, no drift between multiple cores and low amount of time per benchmark - so even TSC with dynamic frequency should be good enough here, but it's good to have it.

Additionally it's worth to check whether we have `rdtscp` flag, which is needed for sequential TSC read.

### Compilers

Clang13.0.0 compiler is downloaded from clang project repository: https://github.com/llvm/llvm-project/releases/download/llvmorg-13.0.0/clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-20.04.tar.xz

GCC11.2.0 compiler is downloaded from my repository: https://gitlab.com/PanOrka/sx86it-docker-registry/-/raw/master/gcc-11.2.0-x86_64-linux-gnu-ubuntu-20.04.tar.xz

GCC11.2.0 is provided as pre-built binaries for x86-64, I've compiled them with configure-options:
```
--host=x86_64-pc-linux-gnu --disable-multilib --target=x86_64-pc-linux-gnu --enable-languages=c++ --disable-bootstrap
```

### Docker Image

Docker image is kept on separate repository with gcc compiler: https://gitlab.com/PanOrka/sx86it-docker-registry

`<sx86it-repo-root>/deploy/DOCKER_REGISTRY` uses image from that repository and `DOCKER_REGISTRY` is used by `makesdk.sh` and `deploy_containers.sh` scripts.

Repository has latest used version by me, but you are free to build your own docker image with `<sx86it-repo-root>/docker_build` scripts and use different registry through `DOCKER_REGISTRY`.
